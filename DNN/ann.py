import keras
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense


#initializing the ANN

dataset = pd.read_csv('data_set.csv')

X = dataset.iloc[:, 0:65].values
y = dataset.iloc[:, 65:66].values


from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.2,random_state=0)


ksa32 = Sequential()

ksa32.add(Dense(input_dim=65, activation="relu", units=512, kernel_initializer="uniform"))


ksa32.add(Dense(activation="relu", units=512, kernel_initializer="uniform"))



ksa32.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))

# Compliling the ANN

ksa32.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

#fitting the ANN to the training set

ksa32.fit(X_train, y_train,batch_size=10,epochs=200)

y_pred = ksa32.predict(X_test)
y_pred = (y_pred>0.5)


from sklearn.metrics import confusion_matrix                                                             
cm = confusion_matrix(y_test,y_pred)

ksa32.save('ksa32.h5')
