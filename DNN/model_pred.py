import keras
import numpy as np
import pandas as pd
from keras.models import Sequential
from keras.layers import Dense
from keras.models import load_model

file1 = open("pred.txt","w")

model = load_model('ksa32.h5')

model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

dataset = pd.read_csv('new_dataset.csv')

X = dataset.iloc[:, 0:65].values
y = dataset.iloc[:, 65:66].values

classes = model.predict_classes(X)

out = classes.tolist()
data = str(out)

out_y = y.tolist()

case1=0
case2=0

for t in range(len(out)):
	if(out[t][0]==0 and out_y[t][0]==0):
		case1 = case1+1
	if(out[t][0]==1 and out_y[t][0]==0):
		case2 = case2+1

print('case1',case1)
print('case2',case2)
		

file1.write(data)

